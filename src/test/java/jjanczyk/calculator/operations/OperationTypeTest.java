package jjanczyk.calculator.operations;

import jjanczyk.calculator.exceptions.IncorrectOperationTypeException;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by Jakub Janczyk on 2015-08-08.
 */
public class OperationTypeTest {

    @Test
    public void testReturningValueFromString() {
        OperationType addType = OperationType.valueFromString("add");
        OperationType multiplyType = OperationType.valueFromString("multiply");

        assertEquals(OperationType.ADD, addType);
        assertEquals(OperationType.MULTIPLY, multiplyType);
    }

    @Test
    public void testValueFromStringCaseInsensitive() {
        assertEquals(OperationType.ADD, OperationType.valueFromString("Add"));
        assertEquals(OperationType.ADD, OperationType.valueFromString("aDd"));
        assertEquals(OperationType.ADD, OperationType.valueFromString("adD"));
        assertEquals(OperationType.ADD, OperationType.valueFromString("ADD"));
    }

    @Test(expected = IncorrectOperationTypeException.class)
    public void testThrowingExceptionForIncorrectOperation() {
        OperationType.valueFromString("not a correct operation type");
    }

}
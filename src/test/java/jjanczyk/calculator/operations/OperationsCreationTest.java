package jjanczyk.calculator.operations;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by Jakub Janczyk on 2015-08-08.
 */
public class OperationsCreationTest {

    @Test
    public void testAddOperation() {
        Operation addOperation = OperationType.ADD.createOperation();
        assertEquals(4, addOperation.calculate(2, 2));
        assertEquals(8, addOperation.calculate(3, 5));
        assertEquals(0, addOperation.calculate(2, -2));
        assertEquals(12, addOperation.calculate(11, 1));
    }

    @Test
    public void testMultiplyOperation() {
        Operation addOperation = OperationType.MULTIPLY.createOperation();
        assertEquals(4, addOperation.calculate(2, 2));
        assertEquals(8, addOperation.calculate(4, 2));
        assertEquals(15, addOperation.calculate(3, 5));
        assertEquals(0, addOperation.calculate(11, 0));
        assertEquals(-10, addOperation.calculate(10, -1));
    }

    @Test
    public void testApplyOperation() {
        Operation applyOperation = OperationType.APPLY.createOperation();
        assertEquals(2, applyOperation.calculate(0, 2));
        assertEquals(3, applyOperation.calculate(0, 3));
        assertEquals(0, applyOperation.calculate(0, 0));
        assertEquals(-5, applyOperation.calculate(0, -5));
    }

    @Test
    public void testSubtractOperation() {
        Operation addOperation = OperationType.SUBTRACT.createOperation();
        assertEquals(4, addOperation.calculate(6, 2));
        assertEquals(8, addOperation.calculate(18, 10));
        assertEquals(15, addOperation.calculate(16, 1));
        assertEquals(11, addOperation.calculate(11, 0));
        assertEquals(11, addOperation.calculate(10, -1));
    }

    @Test
    public void testDivideOperation() {
        Operation addOperation = OperationType.DIVIDE.createOperation();
        assertEquals(1, addOperation.calculate(2, 2));
        assertEquals(2, addOperation.calculate(4, 2));
        assertEquals(3, addOperation.calculate(15, 5));
        assertEquals(-10, addOperation.calculate(10, -1));
    }
}

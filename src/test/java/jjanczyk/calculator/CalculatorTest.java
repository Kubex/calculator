package jjanczyk.calculator;

import jjanczyk.calculator.parser.SimpleLinesParser;
import jjanczyk.calculator.parser.Parser;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by Jakub Janczyk on 2015-08-09.
 */
public class CalculatorTest {

    private Parser parser = new SimpleLinesParser();
    private Calculator calculator = new Calculator(parser);

    @Test
    public void testCalculationWithResult36() {
        List<String> lines = Arrays.asList("add 2", "multiply 3", "apply 10");

        int result = calculator.performCalculation(lines);

        assertEquals(36, result);
    }

    @Test
    public void testCalculationWithResult32() {
        List<String> lines = Arrays.asList("multiply 3", "add 2", "apply 10");

        int result = calculator.performCalculation(lines);

        assertEquals(32, result);
    }

    @Test
    public void testCalculationWithResult1() {
        List<String> lines = Collections.singletonList("apply 1");

        int result = calculator.performCalculation(lines);

        assertEquals(1, result);
    }

    @Test
    public void testCalculationWithSubtract() {
        List<String> lines = Arrays.asList("multiply 3", "subtract 2", "apply 10");

        int result = calculator.performCalculation(lines);

        assertEquals(28, result);
    }

    @Test
    public void testCalculationWithDivide() {
        List<String> lines = Arrays.asList("multiply 3", "divide 2", "apply 10");

        int result = calculator.performCalculation(lines);

        assertEquals(15, result);
    }

}
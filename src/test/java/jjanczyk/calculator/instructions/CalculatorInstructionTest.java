package jjanczyk.calculator.instructions;

import jjanczyk.calculator.instructions.CalculatorInstruction;
import jjanczyk.calculator.operations.OperationType;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Jakub Janczyk on 2015-08-09.
 */
public class CalculatorInstructionTest {

    @Test
    public void testAddOperationExecuted() {
        CalculatorInstruction instruction = new CalculatorInstruction(10, OperationType.ADD);

        assertEquals(15, instruction.execute(5));
        assertEquals(21, instruction.execute(11));
    }

    @Test
    public void testMultiplyOperationExecuted() {
        CalculatorInstruction instruction = new CalculatorInstruction(4, OperationType.MULTIPLY);

        assertEquals(20, instruction.execute(5));
        assertEquals(32, instruction.execute(8));
    }

    @Test
    public void testApplyOperationExecuted() {
        CalculatorInstruction instruction = new CalculatorInstruction(10, OperationType.APPLY);

        assertEquals(10, instruction.execute(0));
        assertEquals(10, instruction.execute(4));
    }

}
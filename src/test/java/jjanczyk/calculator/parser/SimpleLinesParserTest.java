package jjanczyk.calculator.parser;

import jjanczyk.calculator.instructions.CalculatorInstruction;
import jjanczyk.calculator.exceptions.LineFormatException;
import jjanczyk.calculator.exceptions.IncorrectOperationTypeException;
import jjanczyk.calculator.exceptions.MissingApplyException;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by Jakub Janczyk on 2015-08-09.
 */
public class SimpleLinesParserTest {

    private SimpleLinesParser parser = new SimpleLinesParser();

    @Test
    public void testCorrectParsingTwoLines() {
        List<String> lines = Arrays.asList("add 10", "apply 2");
        List<CalculatorInstruction> instructions = parser.parse(lines);

        assertEquals(2, instructions.size());
        assertEquals(2, instructions.get(0).execute(0));
        assertEquals(12, instructions.get(1).execute(2));
    }

    @Test
    public void testCorrectParsingThreeLines() {
        List<String> lines = Arrays.asList("add 10", "multiply 3", "apply 2");
        List<CalculatorInstruction> instructions = parser.parse(lines);

        assertEquals(3, instructions.size());
        assertEquals(2, instructions.get(0).execute(0));
        assertEquals(12, instructions.get(1).execute(2));
        assertEquals(6, instructions.get(2).execute(2));
    }

    @Test
    public void testCorrectParseWithOnlyApply() {
        List<String> lines = Collections.singletonList("apply 10");
        List<CalculatorInstruction> instructions = parser.parse(lines);

        assertEquals(1, instructions.size());
        assertEquals(10, instructions.get(0).execute(0));
    }

    @Test(expected = LineFormatException.class)
    public void testIncorrectLineWithMissingValue() {
        List<String> lines = Arrays.asList("add", "apply 2");
        parser.parse(lines);
    }

    @Test(expected = LineFormatException.class)
    public void testIncorrectLineWithTooMuchTokens() {
        List<String> lines = Arrays.asList("add this 2", "apply 2");
        parser.parse(lines);
    }

    @Test(expected = MissingApplyException.class)
    public void testMissingApply() {
        List<String> lines = Collections.singletonList("add 10");
        parser.parse(lines);
    }

    @Test(expected = MissingApplyException.class)
    public void testApplyNotLastInstruction() {
        List<String> lines = Arrays.asList("apply 2", "add 10");
        parser.parse(lines);
    }

    @Test(expected = MissingApplyException.class)
    public void testEmptyInput() {
        List<String> lines = Collections.emptyList();
        parser.parse(lines);
    }

    @Test(expected = IncorrectOperationTypeException.class)
    public void testIncorrectInstructionType() {
        List<String> lines = Arrays.asList("notCorrectInstruction 2", "apply 10");
        parser.parse(lines);
    }

    @Test(expected = NumberFormatException.class)
    public void testValueIsNotInt() {
        List<String> lines = Arrays.asList("add s", "apply 10");
        parser.parse(lines);
    }


}
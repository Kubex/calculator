package jjanczyk.calculator.exceptions;

/**
 * Created by Jakub Janczyk on 2015-08-09.
 */
public class LineFormatException extends RuntimeException {

    public LineFormatException() {
    }

    public LineFormatException(String s) {
        super(s);
    }
}

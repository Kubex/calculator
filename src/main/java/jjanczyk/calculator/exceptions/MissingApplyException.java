package jjanczyk.calculator.exceptions;

/**
 * Created by Jakub Janczyk on 2015-08-09.
 */
public class MissingApplyException extends RuntimeException {
    public MissingApplyException() {
    }

    public MissingApplyException(String s) {
        super(s);
    }
}

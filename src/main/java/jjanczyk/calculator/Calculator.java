package jjanczyk.calculator;

import jjanczyk.calculator.instructions.CalculatorInstruction;
import jjanczyk.calculator.parser.Parser;

import java.util.List;

/**
 * Created by Jakub Janczyk on 2015-08-09.
 */
public class Calculator {
    private final Parser parser;

    public Calculator(Parser parser) {
        this.parser = parser;
    }

    /**
     * Calculates the result, based on given lines with instructions.
     * <p>
     * List of instructions is iterated over and result of each instruction is used as input for the next one
     * (with 0 as the starting value - first instruction has to be "apply")
     *
     * @param lines - list of input file lines, with format that is accepted by specified parser instance.
     * @return final result.
     */
    public int performCalculation(List<String> lines) {
        List<CalculatorInstruction> instructions = parser.parse(lines);

        int currentResult = 0;
        for (CalculatorInstruction instruction : instructions) {
            currentResult = instruction.execute(currentResult);
        }

        return currentResult;
    }
}

package jjanczyk.calculator.instructions;

import jjanczyk.calculator.operations.Operation;
import jjanczyk.calculator.operations.OperationType;

/**
 * New instance of this class is created for each instruction from input file.
 * They can be used to sequentially execute each instruction, by passing current value for which operation is to be performed.
 * <p>
 * Created by Jakub Janczyk on 2015-08-08.
 */
public class CalculatorInstruction {
    private final int nextValue;
    private final OperationType operationType;
    private final Operation operation;

    /**
     * @param nextValue value to apply with given operation
     * @param operationType type of operation done by this instruction
     */
    public CalculatorInstruction(int nextValue, OperationType operationType) {
        this.nextValue = nextValue;
        this.operationType = operationType;
        this.operation = operationType.createOperation();
    }

    /**
     * Executes current operation with current value as first component and internal next value as second.
     *
     * @param valueToApplyWith value which will be the first component of specified operation.
     *                         Should be the result of previous operation, 0 if this is first operation.
     * @return result of computation. Might use it as input for the next instruction.
     */
    public int execute(int valueToApplyWith) {
        return this.operation.calculate(valueToApplyWith, nextValue);
    }

    public boolean isApplyOperation() {
        return this.operationType == OperationType.APPLY;
    }
}

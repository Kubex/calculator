package jjanczyk.calculator.parser;

import jjanczyk.calculator.instructions.CalculatorInstruction;

import java.util.List;

/**
 * Created by Jakub Janczyk on 2015-08-08.
 */
public interface Parser {
    List<CalculatorInstruction> parse(List<String> lines);
}

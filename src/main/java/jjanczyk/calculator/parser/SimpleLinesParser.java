package jjanczyk.calculator.parser;

import jjanczyk.calculator.instructions.CalculatorInstruction;
import jjanczyk.calculator.exceptions.LineFormatException;
import jjanczyk.calculator.exceptions.MissingApplyException;
import jjanczyk.calculator.operations.OperationType;

import java.util.LinkedList;
import java.util.List;

/**
 * Parses passed lines with specified format, examples of correct lines:
 *
 * add 10
 * apply 1
 * subtract 15
 * multiply 12
 *
 * Examples of incorrect lines:
 *
 * add
 * 1
 * add sdf
 * notSupportedOperation 3
 *
 * Last line has to contain "apply" operation.
 *
 * Created by Jakub Janczyk on 2015-08-09.
 */
public class SimpleLinesParser implements Parser {

    /**
     * Parses given lines with rules specified in Class JavaDoc.
     * @param lines lines to be parsed.
     * @return list of parsed instructions
     * @throws LineFormatException if line is incorrect
     * @throws MissingApplyException if last instruction isn't "apply"
     */
    @Override
    public List<CalculatorInstruction> parse(List<String> lines) {
        List<CalculatorInstruction> instructions = new LinkedList<>();

        CalculatorInstruction instruction = null;
        for (String line : lines) {
            instruction = createInstruction(line);
            instructions.add(instruction);
        }

        validateApplyIsLastInstruction(instruction);

        moveApplyToBeginning(instructions, instruction);

        return instructions;
    }

    private CalculatorInstruction createInstruction(String line) {
        String[] splitLine = line.split(" ");
        if (splitLine.length != 2) {
            throw new LineFormatException("Incorrect line format - there should be only 2 elements in each line");
        }

        OperationType operationType = OperationType.valueFromString(splitLine[0]);
        int value = Integer.parseInt(splitLine[1]);

        return new CalculatorInstruction(value, operationType);
    }

    private void validateApplyIsLastInstruction(CalculatorInstruction instruction) {
        if (instruction == null || !instruction.isApplyOperation()) {
            throw new MissingApplyException("Apply instruction has to be in the last line");
        }
    }

    private void moveApplyToBeginning(List<CalculatorInstruction> instructions, CalculatorInstruction instruction) {
        instructions.remove(instruction);
        instructions.add(0, instruction);
    }
}

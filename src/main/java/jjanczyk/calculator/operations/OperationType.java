package jjanczyk.calculator.operations;

import jjanczyk.calculator.exceptions.IncorrectOperationTypeException;

/**
 * Specifies all possible operations, with methods to create proper implementation of Operation interface.
 * <p>
 * Created by Jakub Janczyk on 2015-08-08.
 */
public enum OperationType {
    ADD {
        @Override
        public Operation createOperation() {
            return (a, b) -> a + b;
        }
    }, MULTIPLY {
        @Override
        public Operation createOperation() {
            return (a, b) -> a * b;
        }
    }, APPLY {
        @Override
        public Operation createOperation() {
            return (a, b) -> b;
        }
    }, SUBTRACT {
        @Override
        public Operation createOperation() {
            return (a, b) -> a - b;
        }
    }, DIVIDE {
        @Override
        public Operation createOperation() {
            return (a, b) -> a / b;
        }
    };

    /**
     * Get operation type from string - search is case insensitive.
     * @param operation name of operation.
     * @return found enum value.
     * @throws IncorrectOperationTypeException if no operation type with given name exists.
     */
    public static OperationType valueFromString(String operation) {
        for (OperationType operationType : OperationType.values()) {
            if (operation.equalsIgnoreCase(operationType.name())) {
                return operationType;
            }
        }
        throw new IncorrectOperationTypeException();
    }

    /**
     * Factory method to create proper implementation of Operation interface for given type.
     */
    public abstract Operation createOperation();
}

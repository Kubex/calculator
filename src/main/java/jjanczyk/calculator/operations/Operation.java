package jjanczyk.calculator.operations;

/**
 * Implementations of this interface will represent specific operations (e.g. addition, subtraction, etc.)
 * <p>
 * Created by Jakub Janczyk on 2015-08-08.
 */
@FunctionalInterface
public interface Operation {
    int calculate(int first, int second);
}

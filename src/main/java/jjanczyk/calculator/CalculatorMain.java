package jjanczyk.calculator;

import jjanczyk.calculator.parser.SimpleLinesParser;
import jjanczyk.calculator.parser.Parser;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

/**
 * Execute with path to the file with instructions to calculate result.
 *
 * For file format, see documentation of currently specified Parser implementation.
 *
 * Created by Jakub Janczyk on 2015-08-09.
 */
public class CalculatorMain {

    public static void main(String[] args) throws IOException {

        if (args.length == 0) {
            throw new IllegalArgumentException("Specify path for the input file!");
        }

        Parser parser = new SimpleLinesParser();
        Calculator calculator = new Calculator(parser);

        List<String> fileLines = Files.readAllLines(Paths.get(args[0]));

        int result = calculator.performCalculation(fileLines);

        System.out.println("Result: " + result);
    }
}

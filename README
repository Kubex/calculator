Calculator reads set of instructions from file, parses them and executes in correct order, producing final result.

*File format:*
File with instructions has to look for example as:

add 1
multiply 2
apply 13

Each line contains 2 "tokens" - type of operation and value to apply with this operation (list of currently implemented operations is below).
The last instruction must contain "apply" operation, otherwise exception will be thrown.
If any line is not in this format, exception will be thrown.

*List of currently implemented operations:*
- add
- multiply
- subtract
- divide
- apply

To add new operation, one need to add new value in OperationType enum, along with proper implementation of Operation interface.

*Additional assumptions*
- Main assumption is that calculator operates only on integer values

*Project flow*
Calculator performs set of operations to create final result:
- reads lines from given file
- parses those lines and produces list of CalculatorInstruction objects that will be used to create final result (Parser interface)
- instructions from previous step are iterated over sequentially and executed, with current, cummulated result as input for the next instruction (Calculator class)
- final result is printed to console.

*How to run*
Use 'mvn package' to create JAR file.
Then go to target folder and use 'java -jar calculator-1.0.jar filePath' to run the calculator. Result will be printed to the console.

filePath is a path to the file that contains instruction with described format.

I've prepared few example files, they can be found in src/main/java/resources.
To run them, after building jar in target folder, use, for example: 'java -jar calculator-1.0.jar classes/result15.txt'